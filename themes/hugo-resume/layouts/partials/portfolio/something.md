SOMETHING
{{ range .Site.Data.education }}
{{ .school | markdownify }}
{{ .degree }}
{{ .major | markdownify }}
{{ .notes | markdownify }} 
{{ .range }} 
{{ end }} 