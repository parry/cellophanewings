(async function() {
  console.log('loading /ParryKietzmanCV.pdf')
  const pdfDoc = await pdfjsLib.getDocument('/ParryKietzmanCV.pdf').promise

  const {numPages} = pdfDoc;
  console.log(`loading ${numPages} pages`)

  const pdfPages = await Promise.all(Array.from(
    {length: numPages},
    (_, i)=> pdfDoc.getPage(i + 1)
  ))

  const pagesContainer = document.getElementById("cv-pages")

  for (let pdfPage of pdfPages) {
    console.log(`rendering page ${pdfPage.pageNumber}`)

    const canvas = document.createElement('canvas')
    canvas.style.width = '100%'
    pagesContainer.appendChild(canvas)

    const viewport = pdfPage.getViewport({ scale: 2.0 })
    canvas.width = viewport.width
    canvas.height = viewport.height

    const canvasContext = canvas.getContext("2d")
    await pdfPage.render({ canvasContext, viewport }).promise
  }

  console.log('finished')
})()
